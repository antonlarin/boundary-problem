#include <cmath>
#include <cstdio>

#include "bpsolver.h"

#include "firsttestproblem.h"
#include "secondtestproblem.h"
#include "primaryproblem.h"

using namespace tdmtools;
using namespace bputils;

using namespace bpsolver;

extern "C"
{

BPTask* initTask(uint32_t gridDim, ProblemType problemType)
{
  BPTask* task = new BPTask();
  task->problemType = problemType;
  switch (problemType)
  {
  case FIRST_TEST_PROBLEM:
    task->problem = new FirstTestProblem(gridDim);
    break;
  case SECOND_TEST_PROBLEM:
    task->problem = new SecondTestProblem(gridDim);
    break;
  default: // PRIMARY_PROBLEM
    task->problem = new PrimaryProblem(gridDim);
    break;
  }
  return task;
}

void destroyTask(BPTask*& task)
{
  delete task->problem;
  delete task;
  task = nullptr;
}

void solveTask(BPTask*& task)
{
  task->problem->solve();
}

bool isPrimaryProblem(const BPTask* task)
{
  return task->problemType == PRIMARY_PROBLEM;
}

const double* getNumericalSolution(const BPTask* task)
{
  return task->problem->getSecondSolution().data();
}

const double* getAnalyticalSolution(const BPTask* task)
{
  return task->problem->getFirstSolution().data();
}

const double* getSolutionsDifference(const BPTask* task)
{
  return task->problem->getDeviations().data();
}

double getMaxDeviation(const BPTask* task)
{
  return task->problem->getMaxDeviationData().fOfX;
}

double getMaxDeviationX(const BPTask* task)
{
  return task->problem->getMaxDeviationData().x;
}

uint32_t getGridDimension(const BPTask* task)
{
  return task->problem->getGridDimension();
}

const double* getGrid(const BPTask* task)
{
  return task->problem->getGrid().data();
}

} // extern "C"

