#ifndef PROBLEMSPEC_H
#define PROBLEMSPEC_H

#include <cmath>

#include "bpsolver.h"

namespace bpsolver
{

struct ProblemSpec
{
  ProblemSpec(const array& aCoeffs, const array& dCoeffs, const array& phiCoeffs):
    as(aCoeffs), ds(dCoeffs), phis(phiCoeffs) {}

  array as;
  array ds;
  array phis;
};

class ProblemSpecProvider
{
public:
  static const ProblemSpec* getFirstTestProblemSpec(BPTask* task)
  {
    uint32_t n = getGridDimension(task);
    double gridStep = 1.0 / n;
    
    array aCoeffs;
    double segmentLeftEnd = 0.0;
    double segmentRightEnd = gridStep;
    double a1 = 1.0 + 1.0 / sqrt(2.0);
    double a2 = 0.5;
    for (uint32_t i = 0; i < n; ++i)
    {
      if (segmentRightEnd < xi())
        aCoeffs.emplace_back(a1);
      else if (segmentLeftEnd > xi())
        aCoeffs.emplace_back(a2);
      else
        aCoeffs.emplace_back(gridStep / ((xi() - segmentLeftEnd)/a1 +
                             (segmentRightEnd - xi())/a2));
      
      segmentLeftEnd = segmentRightEnd;
      segmentRightEnd += gridStep;
    }

    array dCoeffs;
    array phiCoeffs;
    double shiftedSegmentLeftEnd = gridStep / 2.0;
    double shiftedSegmentRightEnd = shiftedSegmentLeftEnd + gridStep;
    double d1 = 1.0;
    double d2 = xi() * xi() * 2;
    double phi1 = 1.0;
    double phi2 = 1.0 / sqrt(2.0);
    for (uint32_t i = 0; i < n - 1; ++i)
    {
      if (shiftedSegmentRightEnd < xi())
      {
        dCoeffs.emplace_back(d1);
        phiCoeffs.emplace_back(phi1);
      }
      else if (shiftedSegmentLeftEnd > xi())
      {
        dCoeffs.emplace_back(d2);
        phiCoeffs.emplace_back(phi2);
      }
      else
      {
        dCoeffs.emplace_back((d1*(xi() - shiftedSegmentLeftEnd) + 
                             d2*(shiftedSegmentRightEnd - xi())) / gridStep);
        phiCoeffs.emplace_back((phi1*(xi() - shiftedSegmentLeftEnd) +
                               phi2*(shiftedSegmentRightEnd - xi())) / gridStep);
      }

      shiftedSegmentLeftEnd = shiftedSegmentRightEnd;
      shiftedSegmentRightEnd += gridStep;
    }

    return new ProblemSpec(aCoeffs, dCoeffs, phiCoeffs);
  }

  static inline double xi() { return M_PI / 4.0; }
};
  
} // namespace bpsolver

#endif // PROBLEMSPEC_H
