#ifndef SECONDTESTPROBLEM_H
#define SECONDTESTPROBLEM_H

#include "problem.h"

class SecondTestProblem : public Problem
{
public:
  explicit SecondTestProblem(uint32_t gridDimension);
  virtual ~SecondTestProblem() {}
  
  virtual void solve();

  virtual const array& getDeviations();

protected:
  virtual void computePrerequisites();
  virtual void computeDeviation();
};

#endif // SECONDTESTPROBLEM_H
