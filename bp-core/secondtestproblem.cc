#include "tdmtools.h"

#include "secondtestproblem.h"

using namespace tdmtools;

SecondTestProblem::SecondTestProblem(uint32_t gridDimension):
  Problem(gridDimension)
{
  setLeftBC(-91.0);
  setRightBC(-96.0);

  computePrerequisites();
}

void SecondTestProblem::computePrerequisites() {}

void SecondTestProblem::computeDeviation()
{
  array newDeviation;
  for (uint32_t i = 0; i < getGridDimension() + 1; ++i)
    newDeviation.emplace_back(fabs(getFirstSolution()[i] -
                                   getSecondSolution()[i]));
  setDeviation(newDeviation);
}

void SecondTestProblem::solve()
{
  uint32_t n = getGridDimension();
  double h = getDomainSize() / ((double) n);
  array a(n - 1, 0.0);
  array b(n - 1, 0.0);
  array c(n - 1, 0.0);
  array rhs(n + 1, 0.0);

  for (uint32_t i = 0; i < n - 1; ++i)
  {
    a[i] = 1.0 / (h * h);
    b[i] = 1.0 / (h * h);
    c[i] = 2.0 / (h * h) + 0.5;
    double x = getDomainLeftEnd() + h * (i + 1);
    rhs[i + 1] = -0.5*x*x + 3.0*x + 47.5;
  }
  rhs[0] = getLeftBC();
  rhs[n] = getRightBC();

  tdmatrix mat(new TDMatrix(n + 1, a, b, c, 0.0, 0.0));
  array secondSolution = TDMA(mat.get(), &rhs);

  array firstSolution;
  for (uint32_t i = 0; i < n + 1; ++i)
  {
    double x = getDomainLeftEnd() + h * i;
    double u = x*x - 6.0*x - 91.0;
    firstSolution.emplace_back(u);
  }

  setFirstSolution(firstSolution);
  setSecondSolution(secondSolution);

}

const array& SecondTestProblem::getDeviations()
{
  if (getDeviation().empty())
    computeDeviation();

  return getDeviation();
}
