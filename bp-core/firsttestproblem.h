#ifndef FIRSTTESTPROBLEM_H
#define FIRSTTESTPROBLEM_H

#include "problem.h"

class FirstTestProblem : public Problem
{
public:
  explicit FirstTestProblem(uint32_t gridDimension);
  virtual ~FirstTestProblem() {}

  virtual void solve();

  virtual const array& getDeviations();

protected:
  virtual void computePrerequisites();
  virtual void computeDeviation();
};

#endif // FIRSTTESTPROBLEM_H
