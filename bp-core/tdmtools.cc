#include "tdmtools.h"

using namespace tdmtools;

array tdmtools::TDMA(const TDMatrix* matrix, const array* rhs)
{
  uint32_t n = matrix->matrixSize - 1;
  array result(n + 1, 0.0);

  // direct pass
  array alpha = { matrix->kappa1 };
  array beta = { (*rhs)[0] };
  for (uint32_t i = 1; i < n; ++i)
  {
    double denom = matrix->cs[i - 1] - alpha[i - 1] * matrix->as[i - 1];
    alpha.emplace_back(matrix->bs[i - 1] / denom);
    double newBeta = (-(*rhs)[i] + matrix->as[i - 1] * beta[i - 1]) / denom;
    beta.emplace_back(newBeta);
  }

  // reverse pass
  result[n] = (matrix->kappa2 * beta[n - 1] + (*rhs)[n]) /
    (1.0 - matrix->kappa2 * alpha[n - 1]);
  for (int32_t i = n - 1; i >= 0; --i)
  {
    uint32_t j = (uint32_t)i;
    result[i] = result[i + 1] * alpha[i] + beta[i];
  }

  return result;
}
