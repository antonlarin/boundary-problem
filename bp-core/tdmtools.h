#ifndef TDMTOOLS_H
#define TDMTOOLS_H

#include <cstdint>
#include <memory>

#include "bputils.h"

using namespace bputils;

namespace tdmtools
{

// Forward declaration
struct TDMatrix;
typedef std::unique_ptr<TDMatrix> tdmatrix;

struct TDMatrix
{
  TDMatrix(uint32_t size,
           const array& a, const array& b, const array& c,
           double k1, double k2):
  matrixSize(size), as(a), bs(b), cs(c), kappa1(k1), kappa2(k2) {}

  uint32_t matrixSize;
  array as;
  array bs;
  array cs;
  double kappa1;
  double kappa2;
};

array TDMA(const TDMatrix* matrix, const array* rhs);

} // namespace tdmtools

#endif // TDMTOOLS_H
