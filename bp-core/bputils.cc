#include <cmath>

#include "bputils.h"

using namespace bputils;

double bputils::errorNorm(const array* a, const array* b)
{
  double result = 0.0;
  array::const_iterator ia, ib;
  for (ia = a->begin(), ib = b->begin(); ia != a->end() && ib != b->end();
       ++ia, ++ib)
  {
    result = std::max(result, fabs(*ia - *ib));
  }

  return result;
}
