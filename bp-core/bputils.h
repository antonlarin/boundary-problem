#ifndef BPUTILS_H
#define BPUTILS_H

#include <vector>
#include <cstdint>

namespace bputils
{
  
typedef std::vector<double> array;


static double EPSILON = 1e-15;


// L_infty norm
double errorNorm(const array* a, const array* b);

} // namespace bputils

#endif // BPUTILS_H
