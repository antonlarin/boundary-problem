#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <memory>

#include "../bpsolver.h"

using namespace bpsolver;

namespace bpsolvertest
{

  TEST(BPSolver, ReturnsNonNullPointerWhenInitTask)
  {
    BPTask* task = nullptr;
    task = initTask(2u, FIRST_TEST_PROBLEM);

    ASSERT_NE(nullptr, task);
    destroyTask(task);
  }

  TEST(BPSolver, ReturnsNullPointerToTaskAfterDestruction)
  {
    BPTask* task = initTask(2u, FIRST_TEST_PROBLEM);
    destroyTask(task);

    ASSERT_EQ(nullptr, task);
  }

  TEST(BPSolver, ReturnsCorrectGridDimension)
  {
    uint32_t gridDimension = 5u;
    BPTask* task = initTask(gridDimension, FIRST_TEST_PROBLEM);

    ASSERT_EQ(gridDimension, getGridDimension(task));
    destroyTask(task);
  }

  TEST(BPSolver, ReturnsCorrectGrid)
  {
    uint32_t gridDimension = 5u;
    BPTask* task = initTask(gridDimension, FIRST_TEST_PROBLEM);
    const double* rawGrid = getGrid(task);
    array grid(rawGrid, rawGrid + gridDimension + 1);
    array expectedGrid = { 0.0, 0.2, 0.4, 0.6, 0.8, 1.0 };

    ASSERT_LE(errorNorm(&grid, &expectedGrid), EPSILON);
    destroyTask(task);
  }

} // namespace bpsolvertest

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
