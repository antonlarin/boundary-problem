#include "gtest/gtest.h"

#include "../bpsolver.h"
#include "../problemspec.h"

using namespace bpsolver;

namespace problemspectest
{
class FirstProblemSpecTest: public ::testing::Test
{
protected:
  FirstProblemSpecTest()
  {
    xi = M_PI / 4;
    gridDimension = 5u;
    task = initTask(gridDimension, FIRST_TEST_PROBLEM);
    ps = const_cast<ProblemSpec*>(
      ProblemSpecProvider::getFirstTestProblemSpec(task));
  }
  
  virtual ~FirstProblemSpecTest()
  {
    delete ps;
    destroyTask(task);
  }

  double xi;
  uint32_t gridDimension;
  BPTask* task;
  ProblemSpec* ps;
};

TEST_F(FirstProblemSpecTest, ReturnsCorrectACoefficinetsForFirstTestProblem)
{
  double a1 = 1.0 + 1.0/sqrt(2);
  double amid = 0.2 / ((xi - 0.6)/a1 + 2*(0.8 - xi));
  double a2 = 0.5;
  array expectedACoeffs = { a1, a1, a1, amid, a2 };

  ASSERT_LE(errorNorm(&expectedACoeffs, &(ps->as)), EPSILON);
}

TEST_F(FirstProblemSpecTest, ReturnsCorrectDCoefficinetsForFirstTestProblem)
{
  double d1 = 1.0;
  double dmid = 5.0 * (xi - 0.7 + 2*xi*xi*(0.9 - xi));
  array expectedDCoeffs = { d1, d1, d1, dmid };

  ASSERT_LE(errorNorm(&expectedDCoeffs, &(ps->ds)), EPSILON);
}

TEST_F(FirstProblemSpecTest, ReturnsCorrectPhiCoefficinetsForFirstTestProblem)
{
  double phi1 = 1.0;
  double phimid = 5.0 * (xi - 0.7 + (0.9 - xi)/sqrt(2.0));
  array expectedPhiCoeffs = { phi1, phi1, phi1, phimid };

  ASSERT_LE(errorNorm(&expectedPhiCoeffs, &(ps->phis)), EPSILON);
}

} // namespace problemspectest
