#include "gtest/gtest.h"

#include "../tdmtools.h"
#include "../bputils.h"

using namespace tdmtools;

namespace tdmtoolstest
{
  
TEST(TDMA, ReturnRHSOnTwoByTwoUnaryMatrix)
{
  array empty;
  tdmatrix unaryMatrix(new TDMatrix(2u, empty, empty, empty, 0.0, 0.0));
  array rhs = { 3.0, 4.0 };

  ASSERT_EQ(rhs, TDMA(unaryMatrix.get(), &rhs));
}

TEST(TDMA, SolveSystemWithThreeByThreeMatrix)
{
  array as = { 1.0 };
  array bs = { 1.0 };
  array cs = { -2.0 };
  tdmatrix threeByThreeMatrix(new TDMatrix(3u, as, bs, cs, 0.0, 0.0));
  array rhs = { 7.0, 13.0, 9.0 };
  array expectedResult = { 7.0, -1.5, 9.0 };
  array actualResult = TDMA(threeByThreeMatrix.get(), &rhs);

  ASSERT_LT(errorNorm(&actualResult, &expectedResult),
            EPSILON);
}

TEST(TDMA, SolveSystemWithThreeByThreeMatrixAndNonzeroKappas)
{
  array as = { 1.0 };
  array bs = { 1.0 };
  array cs = { -2.0 };
  tdmatrix threeByThreeMatrixNonzeroKappas(new TDMatrix(3u, as, bs, cs,
                                                        0.5, -0.5));
  array rhs = { 0.0, -9.0, -5.0 };
  array expectedResult = { -1.0, -2.0, -4.0 };
  array actualResult = TDMA(threeByThreeMatrixNonzeroKappas.get(), &rhs);

  ASSERT_LT(errorNorm(&actualResult, &expectedResult), EPSILON);
}

} // namespace tdmtoolstest

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
