#ifndef PRIMARYPROBLEM_H
#define PRIMARYPROBLEM_H

#include "problem.h"

class PrimaryProblem: public Problem
{
public:
  explicit PrimaryProblem(uint32_t gridDimension);
  virtual ~PrimaryProblem() {}
  
  virtual void solve();

  virtual const array& getDeviations();

protected:
  virtual void computePrerequisites();
  virtual void computeDeviation();

private:
  void solveProblemOnPrimaryGrid();
  void solveProblemOnDoubleGrid();
  void computePrimaryGridPrerequisites();
  void computeDoubleGridPrerequisites();

  array doubleGridACoeffs;
  array doubleGridDCoeffs;
  array doubleGridPhiCoeffs;
};

#endif // PRIMARYPROBLEM_H
