#ifndef PROBLEM_H
#define PROBLEM_H

#include <cmath>

#include "bputils.h"

using bputils::array;

struct FunctionPoint
{
  double x;
  double fOfX;
};

class Problem
{
public:
  explicit Problem(uint32_t gridDimension);
  virtual ~Problem() {}

  virtual void solve() = 0;

  virtual const array& getDeviations() = 0;
  FunctionPoint getMaxDeviationData();

  uint32_t getGridDimension() const;
  double getDomainLeftEnd() const { return domainLeftEnd; }
  double getDomainSize() const { return domainSize; }

  const array& getGrid() const;
  const array& getFirstSolution() const;
  const array& getSecondSolution() const;


protected:
  virtual void computePrerequisites() = 0;
  virtual void computeDeviation() = 0;
  
  const array& getACoeffs() const;
  const array& getDCoeffs() const;
  const array& getPhiCoeffs() const;
  const array& getDeviation() const;
  const double getLeftBC() const;
  const double getRightBC() const;
  
  void setACoeffs(const array& newACoeffs);
  void setDCoeffs(const array& newDCoeffs);
  void setPhiCoeffs(const array& newPhiCoeffs);
  void setGrid(const array& grid);
  void setFirstSolution(const array& newFirstSolution);
  void setSecondSolution(const array& newSecondSolution);
  void setDeviation(const array& newDeviation);
  void setLeftBC(double newBC);
  void setRightBC(double newBC);

  static inline double xi() { return M_PI / 4.0; }
  
private:
  const double domainLeftEnd;
  const double domainSize;

  uint32_t gridDimension;
  array aCoeffs;
  array dCoeffs;
  array phiCoeffs;
  array primaryGrid;
  double muLeft, muRight;

  array firstSolution;
  array secondSolution;
  array deviation;
};

#endif // PROBLEM_H
