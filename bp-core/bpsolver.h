#ifndef BPSOLVER_H
#define BPSOLVER_H

#include <cstdint>

#include "bputils.h"
#include "tdmtools.h"
#include "problem.h"

using namespace tdmtools;
using namespace bputils;

namespace bpsolver
{

extern "C"
{

typedef int ProblemType;
#define FIRST_TEST_PROBLEM 113
#define SECOND_TEST_PROBLEM 114
#define PRIMARY_PROBLEM 115

struct BPTask
{
  ProblemType problemType;
  Problem* problem;
};

BPTask* initTask(uint32_t gridDim, ProblemType problemType);

void destroyTask(BPTask*& task);

void solveTask(BPTask*& task);

bool isPrimaryProblem(const BPTask* task);

const double* getNumericalSolution(const BPTask* task);

const double* getAnalyticalSolution(const BPTask* task);

const double* getSolutionsDifference(const BPTask* task);

double getMaxDeviation(const BPTask* task);

double getMaxDeviationX(const BPTask* task);

uint32_t getGridDimension(const BPTask* task);

const double* getGrid(const BPTask* task);

} // extern "C"

} // namespace bpsolver

#endif // BPSOLVER_H
