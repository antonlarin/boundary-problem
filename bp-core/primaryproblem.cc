#include "tdmtools.h"

#include "primaryproblem.h"

using namespace tdmtools;

PrimaryProblem::PrimaryProblem(uint32_t gridDimension):
  Problem(gridDimension)
{
  setLeftBC(1.0);
  setRightBC(0.0);

  computePrerequisites();
}

void PrimaryProblem::computePrerequisites()
{
  computePrimaryGridPrerequisites();
  computeDoubleGridPrerequisites();
}
  
void PrimaryProblem::solve()
{
  solveProblemOnPrimaryGrid();
  solveProblemOnDoubleGrid();
}

void PrimaryProblem::solveProblemOnPrimaryGrid()
{
  uint32_t n = getGridDimension();
  double h = getDomainSize() / ((double) n);
  array a(n - 1, 0.0);
  array b(n - 1, 0.0);
  array c(n - 1, 0.0);
  array rhs(n + 1, 0.0);

  for (uint32_t i = 0; i < n - 1; ++i)
  {
    a[i] = getACoeffs()[i] / h;
    b[i] = getACoeffs()[i + 1] / h;
    c[i] = (getACoeffs()[i] + getACoeffs()[i + 1]) / h + getDCoeffs()[i] * h;
    rhs[i + 1] = -getPhiCoeffs()[i] * h;
  }
  rhs[0] = getLeftBC();
  rhs[n] = getRightBC();

  tdmatrix mat(new TDMatrix(n + 1, a, b, c, 0.0, 0.0));
  array firstSolution = TDMA(mat.get(), &rhs);

  setFirstSolution(firstSolution);
}

void PrimaryProblem::solveProblemOnDoubleGrid()
{
  uint32_t n = getGridDimension() * 2;
  double h = getDomainSize() / ((double) n);
  array a(n - 1, 0.0);
  array b(n - 1, 0.0);
  array c(n - 1, 0.0);
  array rhs(n + 1, 0.0);

  for (uint32_t i = 0; i < n - 1; ++i)
  {
    a[i] = doubleGridACoeffs[i] / h;
    b[i] = doubleGridACoeffs[i + 1] / h;
    c[i] = (doubleGridACoeffs[i] + doubleGridACoeffs[i + 1]) / h +
      doubleGridDCoeffs[i] * h;
    rhs[i + 1] = -doubleGridPhiCoeffs[i] * h;
  }
  rhs[0] = getLeftBC();
  rhs[n] = getRightBC();

  tdmatrix mat(new TDMatrix(n + 1, a, b, c, 0.0, 0.0));
  array secondSolution = TDMA(mat.get(), &rhs);

  setSecondSolution(secondSolution);
}


void PrimaryProblem::computeDeviation()
{
  array newDeviation;

  for (uint32_t i = 0; i < getGridDimension(); ++i)
    newDeviation.emplace_back(fabs(getFirstSolution()[i] -
                                   getSecondSolution()[i * 2]));

  newDeviation.emplace_back(fabs(getFirstSolution().back() -
                                  getSecondSolution().back()));

  setDeviation(newDeviation);
}

const array& PrimaryProblem::getDeviations()
{
  if (getDeviation().empty())
    computeDeviation();

  return getDeviation();
}

void PrimaryProblem::computePrimaryGridPrerequisites()
{
  uint32_t n = getGridDimension();
  double h = 1.0 / ((double) n);

  array newACoeffs;
  double segmentLeftEnd = 0.0;
  double segmentRightEnd = h;
  for (uint32_t i = 0; i < n; ++i)
  {
    if (segmentRightEnd < xi())
      newACoeffs.emplace_back(h /
        (2 * sin(segmentRightEnd * 0.5) /
        (sin(segmentRightEnd * 0.5) + cos(segmentRightEnd * 0.5)) -
        2 * sin(segmentLeftEnd * 0.5) /
        (sin(segmentLeftEnd * 0.5) + cos(segmentLeftEnd * 0.5))));
    else if (segmentLeftEnd > xi())
      newACoeffs.emplace_back(h / (tan(segmentRightEnd) - tan(segmentLeftEnd)));
    else
    {
      double a1 =
        2 * sin(xi() * 0.5) / (sin(xi() * 0.5) + cos(xi() * 0.5)) -
        2 * sin(segmentLeftEnd * 0.5) /
        (sin(segmentLeftEnd * 0.5) + cos(segmentLeftEnd * 0.5));
      double a2 = tan(segmentRightEnd) - tan(xi());
      newACoeffs.emplace_back(h / (a1 + a2));
    }

    segmentLeftEnd = segmentRightEnd;
    segmentRightEnd += h;
  }

  array newDCoeffs;
  array newPhiCoeffs;
  double shiftedSegmentLeftEnd = h / 2.0;
  double shiftedSegmentRightEnd = shiftedSegmentLeftEnd + h;
  for (uint32_t i = 0; i < n - 1; ++i)
  {
    if (shiftedSegmentRightEnd < xi())
    {
      newDCoeffs.emplace_back(
        (shiftedSegmentRightEnd - shiftedSegmentLeftEnd) / h);
      newPhiCoeffs.emplace_back(
        -0.5 * (cos(2.0 * shiftedSegmentRightEnd) -
                cos(2.0 * shiftedSegmentLeftEnd)) / h);
    }
    else if (shiftedSegmentLeftEnd > xi())
    {
      newDCoeffs.emplace_back(
        2.0 / 3.0 * (pow(shiftedSegmentRightEnd, 3.0) -
                     pow(shiftedSegmentLeftEnd, 3.0)) / h);
      newPhiCoeffs.emplace_back(
        (sin(shiftedSegmentRightEnd) - sin(shiftedSegmentLeftEnd)) / h);
    }
    else
    {
      newDCoeffs.emplace_back((xi() - shiftedSegmentLeftEnd +
        2.0 / 3.0 * (pow(shiftedSegmentRightEnd, 3.0) -
                     pow(xi(), 3.0))) / h);
      newPhiCoeffs.emplace_back((
        -0.5 * (cos(2.0 * xi()) - cos(2.0 * shiftedSegmentLeftEnd)) +
        sin(shiftedSegmentRightEnd) - sin(xi())) / h);
    }

    shiftedSegmentLeftEnd = shiftedSegmentRightEnd;
    shiftedSegmentRightEnd += h;
  }

  setACoeffs(newACoeffs);
  setDCoeffs(newDCoeffs);
  setPhiCoeffs(newPhiCoeffs);
}

void PrimaryProblem::computeDoubleGridPrerequisites()
{
  uint32_t n = getGridDimension() * 2;
  double h = 1.0 / ((double) n);

  double segmentLeftEnd = 0.0;
  double segmentRightEnd = h;
  for (uint32_t i = 0; i < n; ++i)
  {
    if (segmentRightEnd < xi())
      doubleGridACoeffs.emplace_back(h /
        (2 * sin(segmentRightEnd * 0.5) /
        (sin(segmentRightEnd * 0.5) + cos(segmentRightEnd * 0.5)) -
        2 * sin(segmentLeftEnd * 0.5) /
        (sin(segmentLeftEnd * 0.5) + cos(segmentLeftEnd * 0.5))));
    else if (segmentLeftEnd > xi())
      doubleGridACoeffs.emplace_back(h /
        (tan(segmentRightEnd) - tan(segmentLeftEnd)));
    else
    {
      double a1 =
        2 * sin(xi() * 0.5) / (sin(xi() * 0.5) + cos(xi() * 0.5)) -
        2 * sin(segmentLeftEnd * 0.5) /
        (sin(segmentLeftEnd * 0.5) + cos(segmentLeftEnd * 0.5));
      double a2 = tan(segmentRightEnd) - tan(xi());
      doubleGridACoeffs.emplace_back(h / (a1 + a2));
    }

    segmentLeftEnd = segmentRightEnd;
    segmentRightEnd += h;
  }

  double shiftedSegmentLeftEnd = h / 2.0;
  double shiftedSegmentRightEnd = shiftedSegmentLeftEnd + h;
  for (uint32_t i = 0; i < n - 1; ++i)
  {
    if (shiftedSegmentRightEnd < xi())
    {
      doubleGridDCoeffs.emplace_back(
        (shiftedSegmentRightEnd - shiftedSegmentLeftEnd) / h);
      doubleGridPhiCoeffs.emplace_back(
        -0.5 * (cos(2.0 * shiftedSegmentRightEnd) -
                cos(2.0 * shiftedSegmentLeftEnd)) / h);
    }
    else if (shiftedSegmentLeftEnd > xi())
    {
      doubleGridDCoeffs.emplace_back(
        2.0 / 3.0 * (pow(shiftedSegmentRightEnd, 3.0) -
                     pow(shiftedSegmentLeftEnd, 3.0)) / h);
      doubleGridPhiCoeffs.emplace_back(
        (sin(shiftedSegmentRightEnd) - sin(shiftedSegmentLeftEnd)) / h);
    }
    else
    {
      doubleGridDCoeffs.emplace_back((xi() - shiftedSegmentLeftEnd +
        2.0 / 3.0 * (pow(shiftedSegmentRightEnd, 3.0) -
                     pow(xi(), 3.0))) / h);
      doubleGridPhiCoeffs.emplace_back((
        -0.5 * (cos(2.0 * xi()) - cos(2.0 * shiftedSegmentLeftEnd)) +
        sin(shiftedSegmentRightEnd) - sin(xi())) / h);
    }

    shiftedSegmentLeftEnd = shiftedSegmentRightEnd;
    shiftedSegmentRightEnd += h;
  }
}


