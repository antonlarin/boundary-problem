#include <algorithm>

#include "problem.h"

using std::max_element;

Problem::Problem(uint32_t gridDimension): gridDimension(gridDimension),
  domainLeftEnd(0.0), domainSize(1.0)
{
  double h = getDomainSize() / ((double) gridDimension);

  for (uint32_t i = 0; i <= gridDimension; ++i)
    primaryGrid.emplace_back(getDomainLeftEnd() + i * h);
}

uint32_t Problem::getGridDimension() const
{
  return gridDimension;
}

const array& Problem::getACoeffs() const
{
  return aCoeffs;
}

const array& Problem::getDCoeffs() const
{
  return dCoeffs;
}

const array& Problem::getPhiCoeffs() const
{
  return phiCoeffs;
}

const array& Problem::getGrid() const
{
  return primaryGrid;
}

const array& Problem::getFirstSolution() const
{
  return firstSolution;
}

const array& Problem::getSecondSolution() const
{
  return secondSolution;
}

const array& Problem::getDeviation() const
{
  return deviation;
}

FunctionPoint Problem::getMaxDeviationData()
{
  if (deviation.empty())
    computeDeviation();

  FunctionPoint maxDeviationData;
  auto maxDeviationIter = max_element(deviation.begin(),
                                      deviation.end());
  maxDeviationData.fOfX = *maxDeviationIter;
  maxDeviationData.x = primaryGrid[maxDeviationIter - deviation.begin()];

  return maxDeviationData;
}

const double Problem::getLeftBC() const
{
  return muLeft;
}

const double Problem::getRightBC() const
{
  return muRight;
}


void Problem::setACoeffs(const array& newACoeffs)
{
  aCoeffs = newACoeffs;
}

void Problem::setDCoeffs(const array& newDCoeffs)
{
  dCoeffs = newDCoeffs;
}

void Problem::setPhiCoeffs(const array& newPhiCoeffs)
{
  phiCoeffs = newPhiCoeffs;
}

void Problem::setGrid(const array& grid)
{
  primaryGrid = grid;
}

void Problem::setFirstSolution(const array& newFirstSolution)
{
  firstSolution = newFirstSolution;
}

void Problem::setSecondSolution(const array& newSecondSolution)
{
  secondSolution = newSecondSolution;
}

void Problem::setDeviation(const array& newDeviation)
{
  deviation = newDeviation;
}

void Problem::setLeftBC(double newBC)
{
  muLeft = newBC;
}

void Problem::setRightBC(double newBC)
{
  muRight = newBC;
}

