#include "tdmtools.h"

#include "firsttestproblem.h"

using namespace tdmtools;

FirstTestProblem::FirstTestProblem(uint32_t gridDimension):
  Problem(gridDimension)
{
  setLeftBC(1.0);
  setRightBC(0.0);

  computePrerequisites();
}

void FirstTestProblem::computePrerequisites()
{
  uint32_t n = getGridDimension();
  double h = 1.0 / ((double) n);

  array newACoeffs;
  double segmentLeftEnd = 0.0;
  double segmentRightEnd = h;
  double a1 = 1.0 + 1.0 / sqrt(2.0);
  double a2 = 0.5;
  for (uint32_t i = 0; i < n; ++i)
  {
    if (segmentRightEnd < xi())
      newACoeffs.emplace_back(a1);
    else if (segmentLeftEnd > xi())
      newACoeffs.emplace_back(a2);
    else
      newACoeffs.emplace_back(h / ((xi() - segmentLeftEnd)/a1 +
                              (segmentRightEnd - xi())/a2));

    segmentLeftEnd = segmentRightEnd;
    segmentRightEnd += h;
  }

  array newDCoeffs;
  array newPhiCoeffs;
  double shiftedSegmentLeftEnd = h / 2.0;
  double shiftedSegmentRightEnd = shiftedSegmentLeftEnd + h;
  double d1 = 1.0;
  double d2 = xi() * xi() * 2;
  double phi1 = 1.0;
  double phi2 = 1.0 / sqrt(2.0);
  for (uint32_t i = 0; i < n - 1; ++i)
  {
    if (shiftedSegmentRightEnd < xi())
    {
      newDCoeffs.emplace_back(d1);
      newPhiCoeffs.emplace_back(phi1);
    }
    else if (shiftedSegmentLeftEnd > xi())
    {
      newDCoeffs.emplace_back(d2);
      newPhiCoeffs.emplace_back(phi2);
    }
    else
    {
      newDCoeffs.emplace_back((d1*(xi() - shiftedSegmentLeftEnd) +
                              d2*(shiftedSegmentRightEnd - xi())) / h);
      newPhiCoeffs.emplace_back((phi1*(xi() - shiftedSegmentLeftEnd) +
                                phi2*(shiftedSegmentRightEnd - xi())) / h);
    }

    shiftedSegmentLeftEnd = shiftedSegmentRightEnd;
    shiftedSegmentRightEnd += h;
  }

  setACoeffs(newACoeffs);
  setDCoeffs(newDCoeffs);
  setPhiCoeffs(newPhiCoeffs);
}

void FirstTestProblem::computeDeviation()
{
  array newDeviation;
  for (uint32_t i = 0; i < getGridDimension() + 1; ++i)
    newDeviation.emplace_back(fabs(getFirstSolution()[i] -
                                   getSecondSolution()[i]));
  setDeviation(newDeviation);
}

void FirstTestProblem::solve()
{
  uint32_t n = getGridDimension();
  double h = getDomainSize() / ((double) n);
  array a(n - 1, 0.0);
  array b(n - 1, 0.0);
  array c(n - 1, 0.0);
  array rhs(n + 1, 0.0);

  for (uint32_t i = 0; i < n - 1; ++i)
  {
    a[i] = getACoeffs()[i] / h;
    b[i] = getACoeffs()[i + 1] / h;
    c[i] = (getACoeffs()[i] + getACoeffs()[i + 1]) / h + getDCoeffs()[i] * h;
    rhs[i + 1] = -getPhiCoeffs()[i] * h;
  }
  rhs[0] = getLeftBC();
  rhs[n] = getRightBC();

  tdmatrix mat(new TDMatrix(n + 1, a, b, c, 0.0, 0.0));
  array secondSolution = TDMA(mat.get(), &rhs);

  double C1 = -0.3787567381181187;
  double C2 = 0.3787567381181187;
  double C3 = -0.22586883718533524;
  double C4 = 2.4695921220659294;

  array firstSolution;
  for (uint32_t i = 0; i < n + 1; ++i)
  {
    double x = getDomainLeftEnd() + h * i;
    double u;
    if (x <= xi())
    {
      double s = sqrt(sqrt(2.0) / (1.0 + sqrt(2.0)));
      u = C1 * exp(s * x) + C2 * exp(-s * x) + 1.0;
    }
    else
    {
      double s = M_PI / 2.0;
      u = C3 * exp(s * x) + C4 * exp(-s * x) + sqrt(2.0) / (s * s);
    }
    firstSolution.emplace_back(u);
  }

  setFirstSolution(firstSolution);
  setSecondSolution(secondSolution);
}

const array& FirstTestProblem::getDeviations()
{
  if (getDeviation().empty())
    computeDeviation();

  return getDeviation();
}
