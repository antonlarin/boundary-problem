#include <algorithm>

#include <QVector>
#include <QStringList>
#include <QSet>

#include "bpguiwindow.h"
#include "ui_bpguiwindow.h"

#include "bpsolver.h"

using std::max_element;

BPGUIWindow::BPGUIWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::BPGUIWindow)
{
    ui->setupUi(this);

    initializeTable(false);
    initializeSolutionsPlot();
    initializeDiffPlot();
    initializeInfo();
}

BPGUIWindow::~BPGUIWindow()
{
    delete ui;
}

void BPGUIWindow::on_solveButton_clicked()
{
    bpsolver::ProblemType type;
    switch (ui->problemComboBox->currentIndex())
    {
    case 0:
        type = FIRST_TEST_PROBLEM;
        break;
    case 1:
        type = SECOND_TEST_PROBLEM;
        break;
    default: // currentIndex == 2
        type = PRIMARY_PROBLEM;
    }

    bpsolver::BPTask* task = bpsolver::initTask(ui->dimensionSpinBox->value(),
                                                type);
    bpsolver::solveTask(task);

    uint32_t n = bpsolver::getGridDimension(task);
    const double* analyticalSolution = bpsolver::getAnalyticalSolution(task);
    const double* numericalSolution = bpsolver::getNumericalSolution(task);
    const double* solutionsDifference = bpsolver::getSolutionsDifference(task);
    const double* grid = bpsolver::getGrid(task);
    QVector<double> xs, analytical, numerical, diff;
    for (uint32_t i = 0; i <= n; ++i)
    {
        xs.push_back(grid[i]);
        analytical.push_back(analyticalSolution[i]);
        if (bpsolver::isPrimaryProblem(task))
            numerical.push_back(numericalSolution[2 * i]);
        else
            numerical.push_back(numericalSolution[i]);
        diff.push_back(solutionsDifference[i]);
    }

    plotSolutions(xs, analytical, numerical,
                  bpsolver::isPrimaryProblem(task));
    replot(ui->solutionsPlotWidget);

    plotDifference(xs, diff, bpsolver::isPrimaryProblem(task));
    replot(ui->diffPlotWidget);

    populateTable(xs, analytical, numerical, diff,
                  bpsolver::isPrimaryProblem(task));

    updateInfo((int)n, bpsolver::getMaxDeviation(task),
               bpsolver::getMaxDeviationX(task),
               bpsolver::isPrimaryProblem(task));

    bpsolver::destroyTask(task);
}

void BPGUIWindow::initializeTable(bool isPrimaryProblem)
{
    ui->tableWidget->setColumnCount(5);
    QStringList tableHeaders;
    tableHeaders << "Индекс" << "X";
    if (isPrimaryProblem)
        tableHeaders << "V1" << "V2" << "|V1 - V2|";
    else
        tableHeaders <<  "U" << "V" << "|U - V|";
    ui->tableWidget->setHorizontalHeaderLabels(tableHeaders);

    while (ui->tableWidget->rowCount() > 0)
        ui->tableWidget->removeRow(0);
}

void BPGUIWindow::initializeSolutionsPlot()
{
    firstSolutionGraph = ui->solutionsPlotWidget->addGraph();
    secondSolutionGraph = ui->solutionsPlotWidget->addGraph();
    firstSolutionGraph->setPen(QPen(Qt::red));
    secondSolutionGraph->setPen(QPen(Qt::green));
    ui->solutionsPlotWidget->xAxis->setLabel("x");
    ui->solutionsPlotWidget->yAxis->setLabel("U, V");
}

void BPGUIWindow::initializeDiffPlot()
{
    differenceGraph = ui->diffPlotWidget->addGraph();
    ui->diffPlotWidget->xAxis->setLabel("x");
    ui->diffPlotWidget->yAxis->setLabel("|U - V|");
}

void BPGUIWindow::plotSolutions(const QVector<double>& xs,
                                const QVector<double>& ys,
                                const QVector<double>& zs,
                                bool isPrimaryProblem)
{
    ui->solutionsPlotWidget->legend->setVisible(true);

    QVector<double> filteredXs, filteredYs, filteredZs;
    int step = xs.length() / MAX_PLOT_POINTS() + 1;

    for (int i = 0; i < xs.length() - 1; i += step)
    {
        filteredXs.push_back(xs[i]);
        filteredYs.push_back(ys[i]);
        filteredZs.push_back(zs[i]);
    }
    filteredXs.push_back(xs.back());
    filteredYs.push_back(ys.back());
    filteredZs.push_back(zs.back());

    if (isPrimaryProblem)
    {
        firstSolutionGraph->setName("Основная сетка");
        secondSolutionGraph->setName("Удвоенная сетка");

        ui->solutionsPlotWidget->yAxis->setLabel("V1, V2");
    }

    else
    {
        firstSolutionGraph->setName("Точное решение");
        secondSolutionGraph->setName("Численное решение");

        ui->solutionsPlotWidget->yAxis->setLabel("U, V");
    }

    firstSolutionGraph->setData(filteredXs, filteredYs);
    secondSolutionGraph->setData(filteredXs, filteredZs);
}

void BPGUIWindow::plotDifference(const QVector<double>& xs,
                                 const QVector<double>& ys,
                                 bool isPrimaryProblem)
{
    if (isPrimaryProblem)
        ui->diffPlotWidget->yAxis->setLabel("|V1 - V2|");
    else
        ui->diffPlotWidget->yAxis->setLabel("|U - V|");

    QVector<double> filteredXs, filteredYs;
    int step = xs.length() / MAX_PLOT_POINTS() + 1;

    for (int i = 0; i < xs.length() - 1; i += step)
    {
        filteredXs.push_back(xs[i]);
        filteredYs.push_back(ys[i]);
    }
    filteredXs.push_back(xs.back());
    filteredYs.push_back(ys.back());

    differenceGraph->setData(filteredXs, filteredYs);
}

void BPGUIWindow::populateTable(const QVector<double>& xs,
                                const QVector<double>& analyticalSolution,
                                const QVector<double>& numericalSolution,
                                const QVector<double>& diff,
                                bool isPrimaryProblem)
{
    initializeTable(isPrimaryProblem);

    int rowCount = std::min(xs.length(), MAX_TABLE_ROWS());
    QList<int> tableRowsIndices;
    int tableSectionMaxRows = MAX_TABLE_ROWS() / 3;

    if (rowCount == xs.length())
    {
        for (int i = 0; i < rowCount; i++)
        tableRowsIndices.push_back(i);
    }
    else
    {
        // First eleven rows
        for (int i = 0; i < std::min(xs.length(), tableSectionMaxRows); ++i)
            tableRowsIndices.push_back(i);
        // Last eleven rows
        for (int i = std::max(0, xs.length() - tableSectionMaxRows);
             i < xs.length(); ++i)
            tableRowsIndices.push_back(i);
        // Eleven rows centered at maximum deviation index
        int maxDeviationIndex = max_element(diff.begin(), diff.end()) -
                diff.begin();
        for (int i = maxDeviationIndex - tableSectionMaxRows / 2;
             i <= maxDeviationIndex + tableSectionMaxRows / 2; ++i)
            tableRowsIndices.push_back(i);

        tableRowsIndices = QSet<int>::fromList(tableRowsIndices).toList();
        std::sort(tableRowsIndices.begin(), tableRowsIndices.end());
    }

    int offset = 0;
    for (uint32_t i = 0; (int)i < tableRowsIndices.size(); ++i)
    {
        int index = tableRowsIndices.at(i);
        int row = i + offset;
        ui->tableWidget->insertRow(row);
        if (i != 0 && index != tableRowsIndices.at(i - 1) + 1)
        {
            QTableWidgetItem* blankItem = new QTableWidgetItem(QString("..."));
            ui->tableWidget->setItem(row, 0, blankItem->clone());
            ui->tableWidget->setItem(row, 1, blankItem->clone());
            ui->tableWidget->setItem(row, 2, blankItem->clone());
            ui->tableWidget->setItem(row, 3, blankItem->clone());
            ui->tableWidget->setItem(row, 4, blankItem->clone());
            delete blankItem;
            offset++;
            row = i + offset;
            ui->tableWidget->insertRow(row);
        }

        QTableWidgetItem* indexItem = new QTableWidgetItem(tr("%1").arg(index));
        ui->tableWidget->setItem(row, 0, indexItem);

        QTableWidgetItem* xItem = new QTableWidgetItem(
                    tr("%1").arg(xs[index], 0, 'g', 4));
        ui->tableWidget->setItem(row, 1, xItem);

        QTableWidgetItem* analyticalSolutionItem = new QTableWidgetItem(
                    tr("%1").arg(analyticalSolution[index], 0, 'g', 4));
        ui->tableWidget->setItem(row, 2, analyticalSolutionItem);

        QTableWidgetItem* numericalSolutionItem = new QTableWidgetItem(
                    tr("%1").arg(numericalSolution[index], 0, 'g', 4));
        ui->tableWidget->setItem(row, 3, numericalSolutionItem);

        QTableWidgetItem* differenceItem = new QTableWidgetItem(
                    tr("%1").arg(diff[index], 0, 'g', 4));
        ui->tableWidget->setItem(row, 4, differenceItem);
    }
}

void BPGUIWindow::replot(QCustomPlot* plotWidget)
{
    plotWidget->xAxis->rescale();
    plotWidget->yAxis->rescale();
    plotWidget->replot();
}

void BPGUIWindow::initializeInfo()
{
    ui->infoLabel->setText("Здесь будет размещена справочная информация по "
                           "решённой задаче.");
    ui->infoLabel->setTextFormat(Qt::RichText);
}

void BPGUIWindow::updateInfo(int n, double maxDeviation, double maxDeviationX,
                             bool isPrimaryProblem)
{
    QString infoText("<strong>%1:</strong><br>"
                     "<em>Число разбиений по x:</em><br>n=%2<br>%3"
                     "<em>%4:</em><br>ε=%5<br>"
                     "<em>%6 наблюдается в точке:</em><br>x=%7");
    if (isPrimaryProblem)
        infoText = infoText.arg("Основная задача").arg(n).
                arg("").arg("Максимальная разность приближённых решений").
                arg(maxDeviation).arg("Она").
                arg(maxDeviationX);
    else
        infoText = infoText.arg("Тестовая задача").arg(n).
                arg("<em>Требуемая точность решения:</em><br>ε=0.5E-6<br>").
                arg("Задача решена с точностью").arg(maxDeviation).
                arg("Максимальное отклонение").arg(maxDeviationX);

    ui->infoLabel->setText(infoText);
}
