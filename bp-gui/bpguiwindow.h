#ifndef BPGUIWINDOW_H
#define BPGUIWINDOW_H

#include <QMainWindow>

#include "qcustomplot.h"

namespace Ui {
class BPGUIWindow;
}

class BPGUIWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit BPGUIWindow(QWidget *parent = 0);
    ~BPGUIWindow();

private slots:
    void on_solveButton_clicked();

private:
    int MAX_PLOT_POINTS() const { return 400; }
    int MAX_TABLE_ROWS() const { return 33; }

    void initializeTable(bool isPrimaryProblem);
    void initializeSolutionsPlot();
    void initializeDiffPlot();
    void initializeInfo();

    void plotSolutions(const QVector<double>& xs, const QVector<double>& ys,
                      const QVector<double>& zs, bool isPrimaryProblem);

    void plotDifference(const QVector<double>& xs, const QVector<double>& ys, bool isPrimaryProblem);

    void replot(QCustomPlot *plotWidget);

    void populateTable(const QVector<double>& xs,
                       const QVector<double>& analyticalSolution,
                       const QVector<double>& numericalSolution,
                       const QVector<double>& diff, bool isPrimaryProblem);

    void updateInfo(int n, double maxDeviation, double maxDeviationX,
                    bool isPrimaryProblem);

    Ui::BPGUIWindow *ui;
    QCPGraph* firstSolutionGraph;
    QCPGraph* secondSolutionGraph;
    QCPGraph* differenceGraph;
};

#endif // BPGUIWINDOW_H
