#include "bpguiwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    BPGUIWindow w;
    w.show();

    return a.exec();
}
