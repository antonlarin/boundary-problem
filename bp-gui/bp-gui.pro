#-------------------------------------------------
#
# Project created by QtCreator 2014-12-01T15:06:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = bp-gui
TEMPLATE = app


SOURCES += main.cpp\
        bpguiwindow.cpp \
    qcustomplot.cpp

HEADERS  += bpguiwindow.h \
    qcustomplot.h

FORMS    += bpguiwindow.ui

unix:!macx: LIBS += -L$$PWD/../bp-core/build/ -lbpcore

INCLUDEPATH += $$PWD/../bp-core
DEPENDPATH += $$PWD/../bp-core

QMAKE_CXXFLAGS += -std=c++11
